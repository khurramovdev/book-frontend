import Vue from "vue"
import VueRouter from "vue-router"
import Home from "../Pages/Home";

Vue.use(VueRouter)

const routes = [
    {
        path: '',
        component: Home,
    },
    {
        path: '/book-info/:id',
        component: () => import("../Pages/BookInfo")
    },
    {
        path: '/login',
        component: () => import("../Pages/Login")
    },
    {
        path: '/by-category/:categoryId',
        component: () => import("../Pages/Home")
    }
]

export default new VueRouter({
    mode: 'history',
    routes
})
