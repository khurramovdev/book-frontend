import axios from "axios";

export default {
    actions: {
        fetchToken(context, data) {
            return new Promise((resolve, reject) => {
                axios.post('http://localhost:8505/api/users/auth', data)
                    .then((response) => {
                        context.commit('updateToken', response.data.token)
                        resolve()
                    })
                    .catch(() => {
                        console.log('tokenni olishda xatolik yuz berdi')
                        reject()
                    })
                    .finally(() => {
                        console.log('finally funksiyasi har doim ishlaydi')
                    })
            });
        }
    },
    mutations: {
        updateToken(state, token) {
            localStorage.setItem('token', token)
            state.token = token
        },
    },
    state: {
        token: localStorage.getItem('token') || '',
    },
    getters: {
        getToken(state) {
            return state.token
        }
    },
}
