import axios from "./axios"

export default {
    actions: {
        fetchCategories(context) {
            return new Promise((resolve, reject) => {
                axios.get('http://localhost:8505/api/categories')
                    .then((response) => {
                        console.log('Janrlar olindi')

                        let data = {
                            models: response.data['hydra:member'],
                            totalItems: response.data['hydra:totalItems'],
                        }

                        context.commit('updateCategories', data)
                        resolve()
                    })
                    .catch(() => {
                        console.log('Janrlarni olishda xatolik yuz berdi')
                        reject()
                    })
            });
        }
    },
    mutations: {
        updateCategories(state, data) {
            state.categories = data
        },
    },
    state: {
        categories: {
            model: [],
            totalItems: 0,
        },
    },
    getters: {
        getCategories(state) {
            return state.categories.models
        }
    },
}
