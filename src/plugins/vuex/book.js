import axios from "./axios"

export default {
    actions: {
        fetchBooks(context, categoryId = null) {
            let category = ''

            if (categoryId) {
                category = '?category=' + categoryId
            }

            return new Promise((resolve, reject) => {
                axios.get('http://localhost:8505/api/books' + category)
                    .then((response) => {
                        console.log('Kitoblar olindi')

                        let data = {
                            models: response.data['hydra:member'],
                            totalItems: response.data['hydra:totalItems'],
                        }

                        context.commit('updateBooks', data)
                        resolve()
                    })
                    .catch(() => {
                        console.log('Kitoblarni olishda xatolik yuz berdi')
                        reject()
                    })
            });
        },
        fetchBook(context, id) {
            return new Promise((resolve, reject) => {
                axios.get('http://localhost:8505/api/books/' + id)
                    .then((response) => {
                        console.log('Kitob olindi')

                        context.commit('updateBook', response.data)
                        resolve()
                    })
                    .catch(() => {
                        console.log('Kitobni olishda xatolik yuz berdi')
                        reject()
                    })
            });
        },
    },
    mutations: {
        updateBooks(state, data) {
            state.books = data
        },
        updateBook(state, data) {
            state.book = data
        }
    },
    state: {
        books: {
            models: [],
            totalItems: 0,
        },
        book: {
            id: null,
            name: null,
            description: null,
            text: null,
            category: null
        }
    },
    getters: {
        getBooks(state) {
            return state.books.models
        },
        getBook(state) {
            return state.book
        },
    },
}
