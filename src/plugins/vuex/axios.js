import axios from "axios"
import store from "./store"

//Har safar so'rov jo'natishdan avval
//Authorization header'ini qo'shamiz.

axios.interceptors.request.use((config) => {
    if (config.url !== 'http://localhost:8505/api/users/auth') {
            config.headers.common['Authorization'] = 'bearer ' + store.getters.getToken
    }

    return config
})

export default axios
